import 'package:flutter/cupertino.dart';
import 'package:foodpappa/data/model/response/cart_model.dart';
import 'package:foodpappa/data/repository/cart_repo.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CartController extends GetxController implements GetxService {
  final CartRepo cartRepo;
  CartController({@required this.cartRepo});
  final List<int> listTipValues = [0,1,5,10,20,30,40,50,100];
  List<CartModel> _cartList = [];
  double _amount = 0.0;
  int ttip = 0;

  List<CartModel> get cartList => _cartList;
  double get amount => _amount;
  int get tip => ttip;

  void getCartData() {
    _cartList = [];
    _cartList.addAll(cartRepo.getCartList());
    _cartList.forEach((cart) {
      _amount = _amount + (cart.discountedPrice * cart.quantity);
    });
  }

  void addToCart(CartModel cartModel, int index) {
    if(index != null) {
      _amount = _amount - (_cartList[index].discountedPrice * _cartList[index].quantity);
      _cartList.replaceRange(index, index+1, [cartModel]);
    }else {
      _cartList.add(cartModel);
    }
    _amount = _amount + (cartModel.discountedPrice * cartModel.quantity);
    cartRepo.addToCartList(_cartList);
    update();
  }

  void setQuantity(bool isIncrement, CartModel cart) {
    int index = _cartList.indexOf(cart);
    if (isIncrement) {
      _cartList[index].quantity = _cartList[index].quantity + 1;
      _amount = _amount + _cartList[index].discountedPrice;
    } else {
      _cartList[index].quantity = _cartList[index].quantity - 1;
      _amount = _amount - _cartList[index].discountedPrice;
    }
    cartRepo.addToCartList(_cartList);

    update();
  }
  void setAdditionalNote(String note, CartModel cart) {
    int index = _cartList.indexOf(cart);
    _cartList[index].additionalNote = note;
    cartRepo.addToCartList(_cartList);
    // update();
  }

  void removeFromCart(int index) {
    _amount = _amount - (_cartList[index].discountedPrice * _cartList[index].quantity);
    _cartList.removeAt(index);
    cartRepo.addToCartList(_cartList);
    update();
  }

  // void showtipPicker(BuildContext ctx,){
  //   showCupertinoModalPopup(
  //       context: ctx,
  //       builder: (_) => Container(
  //         height: 353,
  //         color: Color.fromARGB(255, 255, 255, 255),
  //         child: Column(
  //           children: [
  //             Container(
  //               height: 300,
  //               child: CupertinoPicker(
  //                 children: [
  //                   for(var i in listTipValues)
  //                     Text(i.toString()+"%"),
  //
  //
  //                 ],
  //                 onSelectedItemChanged: (value){
  //                   ttip = listTipValues[value];
  //                   update();
  //                 },
  //                 itemExtent: 25,
  //                 diameterRatio:1,
  //               ),
  //             ),
  //
  //             // Close the modal
  //             CupertinoButton(
  //                 child: Text('OK'),
  //                 onPressed: () async{
  //                   Get.back();
  //
  // }
  //             )
  //           ],
  //         ),
  //       ));
  // }
  int selectedIndex;
  List<int> _options = [10,15,20,];
TextEditingController tipcont = TextEditingController();
  Widget buildChips(BuildContext context) {
    List<Widget> chips = [];

    for (int i = 0; i < _options.length; i++) {
      ChoiceChip choiceChip =  ChoiceChip(
        selected: selectedIndex == i,
        label: Text(" ${_options[i]} %", style: TextStyle(color: selectedIndex==i?Colors.white :Colors.black)),
        // avatar: FlutterLogo(),
        elevation: 2,
        pressElevation: 1,
        shadowColor: Colors.black,
        backgroundColor: Colors.white,
        selectedColor: Theme.of(context).primaryColor,
        onSelected: (bool selected) {
          if (selected) {
              selectedIndex = i;
              update();
            }
          ttip = _options[selectedIndex];
          tipcont.clear();
          update();
        },
      );

      chips.add(choiceChip);
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      // shrinkWrap: true,
      // // This next line does the trick.
      // scrollDirection: Axis.horizontal,
      children: [...chips,Container(
        padding: EdgeInsets.symmetric(vertical: 5),
        width: 130,
        child: TextFormField(
          controller: tipcont,
          onChanged: (val){
            if(val.contains("-")){
              tipcont.clear();
            }else{
              if(val == ""){
                ttip = 0;
              }else{
                ttip = int.parse(val);
              }

              selectedIndex = null;
            }
            update();
          },
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            suffixIcon: Icon(Icons.percent_outlined,size: 18,),
              hintText: "Custom Tip",
          hintStyle: TextStyle(fontSize: 13,fontWeight: FontWeight.w600),
          contentPadding: EdgeInsets.only(left: 10),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))
          ),
        ),
      )],
    );
  }
  void resetTip(){
    ttip = 0;
    update();
  }

  void removeAddOn(int index, int addOnIndex) {
    _cartList[index].addOnIds.removeAt(addOnIndex);
    cartRepo.addToCartList(_cartList);
    update();
  }

  void clearCartList() {
    _cartList = [];
    _amount = 0;
    cartRepo.addToCartList(_cartList);
    update();
  }

  bool isExistInCart(CartModel cartModel, bool isUpdate, int cartIndex) {
    for(int index=0; index<_cartList.length; index++) {
      if(_cartList[index].product.id == cartModel.product.id && (_cartList[index].variation.length > 0 ? _cartList[index].variation[0].type
          == cartModel.variation[0].type : true)) {
        if((isUpdate && index == cartIndex)) {
          return false;
        }else {
          return true;
        }
      }
    }
    return false;
  }

  bool existAnotherRestaurantProduct(int restaurantID) {
    for(CartModel cartModel in _cartList) {
      if(cartModel.product.restaurantId != restaurantID) {
        return true;
      }
    }
    return false;
  }

  void removeAllAndAddToCart(CartModel cartModel) {
    _cartList = [];
    _cartList.add(cartModel);
    _amount = cartModel.discountedPrice * cartModel.quantity;
    cartRepo.addToCartList(_cartList);
    update();
  }


}
