import 'package:flutter/cupertino.dart';
import 'package:foodpappa/controller/cart_controller.dart';
import 'package:foodpappa/controller/coupon_controller.dart';
import 'package:foodpappa/data/model/response/product_model.dart';
import 'package:foodpappa/helper/date_converter.dart';
import 'package:foodpappa/helper/price_converter.dart';
import 'package:foodpappa/helper/responsive_helper.dart';
import 'package:foodpappa/helper/route_helper.dart';
import 'package:foodpappa/util/dimensions.dart';
import 'package:foodpappa/util/styles.dart';
import 'package:foodpappa/view/base/custom_app_bar.dart';
import 'package:foodpappa/view/base/custom_button.dart';
import 'package:foodpappa/view/base/custom_snackbar.dart';
import 'package:foodpappa/view/base/no_data_screen.dart';
import 'package:foodpappa/view/screens/cart/widget/cart_product_widget.dart';
import 'package:flutter/material.dart';
import 'package:foodpappa/view/screens/cart/widget/tip_choice.dart';
import 'package:get/get.dart';

class CartScreen extends StatefulWidget {
  final fromNav;
  CartScreen({@required this.fromNav});

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: 'my_cart'.tr, isBackButtonExist: (ResponsiveHelper.isDesktop(context) || !widget.fromNav)),
      body: GetBuilder<CartController>(
        builder: (cartController) {
          List<List<AddOns>> _addOnsList = [];
          List<bool> _availableList = [];
          double _itemPrice = 0;
          double _addOns = 0;

          // final List<int> listTipValues = [0,1,5,10,20,30,40,50,100];
          // void showtipPicker(BuildContext ctx,){
          //   showCupertinoModalPopup(
          //       context: ctx,
          //       builder: (_) => Container(
          //         height: 353,
          //         color: Color.fromARGB(255, 255, 255, 255),
          //         child: Column(
          //           children: [
          //             Container(
          //               height: 300,
          //               child: CupertinoPicker(
          //                 children: [
          //                   for(var i in listTipValues)
          //                     Text(i.toString()+"%"),
          //
          //
          //                 ],
          //                 onSelectedItemChanged: (value){
          //                   _tipPercentage = listTipValues[value];
          //
          //                 },
          //                 itemExtent: 25,
          //                 diameterRatio:1,
          //               ),
          //             ),
          //
          //             // Close the modal
          //             CupertinoButton(
          //                 child: Text('OK'),
          //                 onPressed: () async{
          //                   Get.back();
          //
          //                 }
          //             )
          //           ],
          //         ),
          //       ));
          // }
          cartController.cartList.forEach((cartModel) {

            List<AddOns> _addOnList = [];
            cartModel.addOnIds.forEach((addOnId) {
              for(AddOns addOns in cartModel.product.addOns) {
                if(addOns.id == addOnId.id) {
                  _addOnList.add(addOns);
                  break;
                }
              }
            });
            _addOnsList.add(_addOnList);

            _availableList.add(DateConverter.isAvailable(cartModel.product.availableTimeStarts, cartModel.product.availableTimeEnds));

            for(int index=0; index<_addOnList.length; index++) {
              _addOns = _addOns + (_addOnList[index].price * cartModel.addOnIds[index].quantity);
            }
            _itemPrice = _itemPrice + (cartModel.price * cartModel.quantity);
          });
          double _subTotal = _itemPrice + _addOns;

          return cartController.cartList.length > 0 ? Column(
            children: [

              Expanded(
                child: Scrollbar(
                  child: SingleChildScrollView(
                    padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL), physics: BouncingScrollPhysics(),
                    child: Center(
                      child: SizedBox(
                        width: Dimensions.WEB_MAX_WIDTH,
                        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [

                          // Product
                          ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: cartController.cartList.length,
                            itemBuilder: (context, index) {
                              return CartProductWidget(cart: cartController.cartList[index], cartIndex: index, addOns: _addOnsList[index], isAvailable: _availableList[index]);
                            },
                          ),
                          SizedBox(height: Dimensions.PADDING_SIZE_SMALL),

                          // Total
                          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                            Text('item_price'.tr, style: robotoRegular),
                            Text(PriceConverter.convertPrice(_itemPrice), style: robotoRegular),
                          ]),
                          SizedBox(height: 10),

                          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                            Text('addons'.tr, style: robotoRegular),
                            Text('(+) ${PriceConverter.convertPrice(_addOns)}', style: robotoRegular),
                          ]),
                          SizedBox(height: 10),

                          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                            Text('ADD A TIP TO YOUR RIDER'.tr, style: robotoBold),
                            // Text("\$ ${(_subTotal*Get.find<CartController>().tip)/100}", style: robotoRegular),
                            InkWell(
                              onTap: (){
                                cartController.ttip=0;
                                cartController.tipcont.clear();
                                cartController.selectedIndex = null;
                                cartController.update();
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(vertical: 8.0),
                                child: Text("CLEAR",style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 15),),
                              ),
                            )

                            // Row(
                            //   children: [
                            //
                            //     // InkWell(
                            //     //   onTap: (){
                            //     //     Get.find<CartController>().showtipPicker(context,);
                            //     //   },
                            //     //   child: Card(
                            //     //     child: Padding(
                            //     //       padding: const EdgeInsets.symmetric(horizontal: 15.0,vertical: 5),
                            //     //       child: Text('${Get.find<CartController>().tip}  %', style: robotoRegular),
                            //     //     ),
                            //     //   ),
                            //     // ),
                            //   ],
                            // )

                          ]),
                          SizedBox(
                            height: 45,
                              width: Get.width,
                              child: cartController.buildChips(context),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: Dimensions.PADDING_SIZE_SMALL),
                            child: Divider(thickness: 1, color: Theme.of(context).hintColor.withOpacity(0.5)),
                          ),

                          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                            Text('subtotal'.tr, style: robotoMedium),
                            Text(PriceConverter.convertPrice(_subTotal+(_subTotal*Get.find<CartController>().tip)/100), style: robotoMedium),
                          ]),


                        ]),
                      ),
                    ),
                  ),
                ),
              ),

              Container(
                width: Dimensions.WEB_MAX_WIDTH,
                padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                child: CustomButton(buttonText: 'proceed_to_checkout'.tr, onPressed: () {
                  if(!cartController.cartList.first.product.scheduleOrder && _availableList.contains(false)) {
                    showCustomSnackBar('one_or_more_product_unavailable'.tr);
                  } else {
                    Get.find<CouponController>().removeCouponData(false);
                    Get.toNamed(RouteHelper.getCheckoutRoute('cart'));
                  }
                }),
              ),

            ],
          ) : NoDataScreen(isCart: true, text: '');
        },
      ),
    );
  }
}
