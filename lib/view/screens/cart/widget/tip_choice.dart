// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:foodpappa/controller/cart_controller.dart';
// import 'package:get/get.dart';
//
// class TipChoice extends StatefulWidget {
//
//   @override
//   TipChoiceState createState() =>
//        TipChoiceState();
// }
//
// class TipChoiceState extends State<TipChoice> with TickerProviderStateMixin {
//
//   int _selectedIndex;
//   List<int> _options = [0,1,5,10,20,30,40,50,100];
//
//   Widget _buildChips() {
//     List<Widget> chips = [];
//
//     for (int i = 0; i < _options.length; i++) {
//       ChoiceChip choiceChip =  ChoiceChip(
//         selected: _selectedIndex == i,
//         label: Text(" ${_options[i]} %", style: TextStyle(color: _selectedIndex==i?Colors.white :Colors.black)),
//         // avatar: FlutterLogo(),
//         elevation: 2,
//         pressElevation: 1,
//         shadowColor: Colors.black,
//         backgroundColor: Colors.white,
//         selectedColor: Theme.of(context).primaryColor,
//         onSelected: (bool selected) {
//           setState(() {
//             if (selected) {
//               _selectedIndex = i;
//             }
//           });
//         },
//       );
//
//       chips.add(Padding(
//           padding: EdgeInsets.symmetric(horizontal: 10),
//           child: choiceChip
//       ));
//     }
//
//     return ListView(
//       shrinkWrap: true,
//       // This next line does the trick.
//       scrollDirection: Axis.horizontal,
//       children: chips,
//     );
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return _buildChips();
//   }
// }