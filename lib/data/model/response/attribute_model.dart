class Attribute {
  int id;
  String name;
  int parentid;
  int position;
  DateTime createdat;
  DateTime updatedat;
  Properties properties;
  Pivot pivot;
  List<Attribute> subAttributes;

  Attribute(
      {this.id,
        this.name,
        this.createdat,this.parentid,this.position,this.updatedat,
        this.properties,
        this.pivot,
        this.subAttributes,
      });

  Attribute.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    parentid = json['parent_id'];
    position = json['position'];
    createdat = DateTime.parse(json['created_at']) ;
    updatedat = DateTime.parse(json['updated_at']);
    if (json['sub_attributes'] != []) {
      subAttributes = [];
      json['sub_attributes'].forEach((v) {
        subAttributes.add( Attribute.fromJson(v));
      });
    }
    if(json['properties']!=null)
    properties = Properties.fromJson(json['properties']);
    if(json['pivot']!=null)
    pivot = Pivot.fromJson(json['pivot']) ;


  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['parent_id'] = this.parentid;
    data['position'] = this.position;
    data['created_at'] = this.createdat;
    data['updated_at'] = this.updatedat;
    if (this.subAttributes != null) {
      data['add_ons'] = this.subAttributes.map((v) => v.toJson()).toList();
    }
    if (this.properties != null) {
      data['add_ons'] = this.properties.toJson();
    }
    return data;
  }
}


class Properties {
  dynamic max;
  String selection;
  String isCaption;

  Properties(
      {
        this.isCaption,
        this.max,
        this.selection,
      });

  Properties.fromJson(Map<String, dynamic> json) {
    max = json['max'];
    isCaption = json['is_caption'];
    selection = json['selection'];



  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['max'] = this.max;
    data['is_caption'] = this.isCaption;
    data['selection'] = this.selection;


    return data;
  }
}
class  Pivot {
  int foodId;
  int attributeId;
  DateTime createdat;
  DateTime updatedat;

  Pivot(
      {
        this.updatedat,
        this.createdat,
        this.attributeId,
        this.foodId,
      });

  Pivot.fromJson(Map<String, dynamic> json) {
    foodId = json['food_id'];
    attributeId = json['attribute_id'];
    createdat = DateTime.parse( json['created_at']);
    updatedat = DateTime.parse(json['updated_at']);



  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['food_id'] = this.foodId;
    data['attribute_id'] = this.attributeId;
    data['created_at'] = this.createdat;
    data['updated_at'] = this.updatedat;



    return data;
  }
}